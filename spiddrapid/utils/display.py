from django.template import engines


def render_jinja2_file(file, context={}):

	jinja2_engine = engines['jinja2']
	template = jinja2_engine.get_template("index.html")
	return template.render(context)
	# pass

def render_jinja2_string(text, context={}):
    if text is None:
        return text
		 
    jinja2_engine = engines['jinja2']
    template = jinja2_engine.from_string(text) 
    try:
        return template.render(context)
    except Exception as e:
        # raise e
        return "Diplay Error"
	