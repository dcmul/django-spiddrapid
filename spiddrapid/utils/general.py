'''
    Created on Feb 27, 2018
    
    @author: dan
    '''
def list_order_by_freq(lst=[]):
    import collections
    counts = collections.Counter(lst)
    new_list = sorted(lst, key=counts.get, reverse=True)
    return list(collections.OrderedDict.fromkeys(new_list))



def extract_richtext(text=''):
    ''' 
     Extract metadata from text and return a dict
    '''    
    import json
    import re
    reglist = re.findall("{metadata}([\s\S]*?){metadata}", text)
    if not reglist:
        print '\nNo meta tags\n'
        return (False, text)
    
    text = re.sub('{metadata}([\s\S]*?){metadata}', '', text)
        
    # take the first one in list
    try:
        metaobject = json.loads(reglist[0])
    except ValueError, e:
        print '\ncant parse json\n'
        return (False, text) 
	
    #TODO check if object is dict
    if not isinstance(metaobject, dict):
        return (False, text)

    return (metaobject, text)

def handle_spidd_replies(msg, metadata):
    import json
    print '\n == herre is uselsse ==\n'

    json_str = metadata['spidd_replies']
    if 'fieldname' in json_str:
        contact = msg.contact
        print '==print contact=='
        print contact
        replyfield = contact.get_field(json_str['fieldname'])
        #quick_replies = msg.metadata['quick_replies']
        
        if not replyfield or not replyfield.string_value:
            return        
        
        quick_replies = json.loads(replyfield.string_value)
        if not isinstance(quick_replies, list):
            return

        metadata.update({'quick_replies':quick_replies})    
        