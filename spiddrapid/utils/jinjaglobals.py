#!/usr/local/bin/python
# -*- coding: utf-8 -*-
from temba.contacts.models import Contact
from temba.utils import json_to_dict
import phonenumbers
import emoji



def get_contacts():
   contacts = Contact.objects.all().values()
   return list(contacts)
   
def fromjson(vstring):
    try:
        return json_to_dict(vstring)
    except ValueError, e:
        return {}

def phone_format(phone, country_code, format):

    x = phonenumbers.parse(phone, country_code)
    return phonenumbers.format_number(x, phonenumbers.PhoneNumberFormat.E164)

def format_text(input_text, style='bold'):

    chars = u'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789. '
    bold_chars = u'𝗔𝗕𝗖𝗗𝗘𝗙𝗚𝗛𝗜𝗝𝗞𝗟𝗠𝗡𝗢𝗣𝗤𝗥𝗦𝗧𝗨𝗩𝗪𝗫𝗬𝗭𝗮𝗯𝗰𝗱𝗲𝗳𝗴𝗵𝗶𝗷𝗸𝗹𝗺𝗻𝗼𝗽𝗾𝗿𝘀𝘁𝘂𝘃𝘄𝘅𝘆𝘇𝟬𝟭𝟮𝟯𝟰𝟱𝟲𝟳𝟴𝟵. '
    # 𝗔𝗕𝗖𝗗𝗘𝗙𝗚𝗛𝗜𝗝𝗞𝗟𝗠𝗡𝗢𝗣𝗤𝗥𝗦𝗧𝗨𝗩𝗪𝗫𝗬𝗭𝗮𝗯𝗰𝗱𝗲𝗳𝗴𝗵𝗶𝗷𝗸𝗹𝗺𝗻𝗼𝗽𝗾𝗿𝘀𝘁𝘂𝘃𝘄𝘅𝘆𝘇𝟬𝟭𝟮𝟯𝟰𝟱𝟲𝟳𝟴𝟵.
    # bold_chars = "𝐀𝐁𝐂𝐃𝐄𝐅𝐆𝐇𝐈𝐉𝐊𝐋𝐌𝐍𝐎𝐏𝐐𝐑𝐒𝐓𝐔𝐕𝐖𝐗𝐘𝐙𝐚𝐛𝐜𝐝𝐞𝐟𝐠𝐡𝐢𝐣𝐤𝐥𝐦𝐧𝐨𝐩𝐪𝐫𝐬𝐭𝐮𝐯𝐰𝐱𝐲𝐳𝟎𝟏𝟐𝟑𝟒𝟓𝟔𝟕𝟖𝟗"
    # bold_chars = '𝔸𝔹ℂ𝔻𝔼𝔽𝔾ℍ𝕀𝕁𝕂𝕃𝕄ℕ𝕆ℙℚℝ𝕊𝕋𝕌𝕍𝕎𝕏𝕐ℤ𝕒𝕓𝕔𝕕𝕖𝕗𝕘𝕙𝕚𝕛𝕜𝕝𝕞𝕟𝕠𝕡𝕢𝕣𝕤𝕥𝕦𝕧𝕨𝕩𝕪𝕫𝟘𝟙𝟚𝟛𝟜𝟝𝟞𝟟𝟠𝟡'

    output = ""
    for character in str(input_text):
        if character in chars:
            output += bold_chars[chars.index(character)]
        else:
            output += character 
    return output    

def end_with_emoji(x):
    # emoji of 2 characters
    if x[:2] in emoji.UNICODE_EMOJI:
        return x[3:].strip()+' ' + x[:2]
    # emoji of 1 character
    if x[:1] in emoji.UNICODE_EMOJI:
        return x[2:].strip()+' ' + x[:1]

    return x

def number_to_button(x):
    number = [u'0️⃣',u'1️⃣',u'2️⃣',u'3️⃣',u'4️⃣',u'5️⃣',u'6️⃣',u'7️⃣',u'8️⃣',u'9️⃣',u'🔟']
    if 0 < x > 10:
        return x
    return number[x]

         
