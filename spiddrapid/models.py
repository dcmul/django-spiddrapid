# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging
from django.db import models
from temba.channels.models import Channel
from temba.msgs.models import Msg
from smartmin.models import SmartModel
from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save, pre_init, post_init
from temba.orgs.models import PLANS, FREE_PLAN
from django.contrib.auth.models import User
from django.conf import settings


from django.utils.translation import ugettext_lazy as _, ungettext_lazy as _n

# Create your models here.
logger = logging.getLogger(__name__)

# class DummyType(Channel):
#     class Meta:
#         proxy = True
# 
#     @classmethod
#     def send_dummy_message(cls, channel, msg, text): 
#     	super(DummyType, cls).send_dummy_message(channel, msg, text)
#     	logger.warn('Unable to get currency for channel countries.', exc_info=True)
#     	print '\n====================overide dummy send method\n======================='


class ResultByContact(models.Model):
    contactid = models.IntegerField(null=False, default=0,
                                    help_text=_("Contact"))
    orgid = models.IntegerField(null=False, default=0,
                                    help_text=_("Organisation"))    
# #     orgid = models.IntegerField(null=False, default=0,
#                                     help_text=_("Organisation"))    
    lastflowrunid = models.IntegerField(default=1, null=False,
                                    help_text=_("Last flow Run"))  
    frequency = models.IntegerField(default=0,
                                    help_text=_("Frequency"))
    fieldkey = models.TextField(null=True, blank=True, default="", 
                                 help_text=_("Fields name"))
    fieldvalue  = models.TextField(null=True, blank=True, default="", 
                                 help_text=_("Fields value"))      
    
    
    class Meta:
        unique_together = (("contactid", "fieldkey", "fieldvalue",),)
                  
    @classmethod 
    def get_lastrunid(cls):
        rec = cls.objects.order_by('-id').first()
        lastid = rec.lastflowrunid if rec  else 1

        return lastid


class Customer(models.Model):
#     contactid = models.IntegerField(null=False, default=0,
    name = models.CharField(verbose_name=_("Name"), max_length=128)
    plan = models.CharField(verbose_name=_("Plan"), max_length=16, choices=PLANS, default=FREE_PLAN,
                            help_text=_("What plan your organization is on"))
    plan_start = models.DateTimeField(verbose_name=_("Plan Start"), auto_now_add=True,
                                      help_text=_("When the user switched to this plan"))

    administrators = models.ManyToManyField(User, verbose_name=_("Administrators"), related_name="customer_admins",
                                            help_text=_("The administrators in your organization"))

    language = models.CharField(verbose_name=_("Language"), max_length=64, null=True, blank=True,
                                choices=settings.LANGUAGES, help_text=_("The main language used by this organization"))

    country = models.ForeignKey('locations.AdminBoundary', null=True, blank=True, on_delete=models.SET_NULL,
                                help_text="The country this organization should map results for.")

    config = models.TextField(null=True, verbose_name=_("Configuration"),
                              help_text=_("More Organization specific configuration"))

    slug = models.SlugField(verbose_name=_("Slug"), max_length=255, null=True, blank=True, unique=True,
                            error_messages=dict(unique=_("This slug is not available")))




# @receiver(post_save, sender=Msg)
# def receiver_function(sender, *args, **kwargs):
# # def fuck_this_shit(sender, instance=None, created=False, **kwargs):
#     # if created:

#     print 'Post save\n|||||||||||||||||'

# @receiver(pre_save, sender=Msg)
# def pre_save(sender, instance, **kwargs):
#     print 'fuckyou'
#     print '\npre-save\n|||||||||||||||||'


# @receiver(pre_save, sender=Msg)
# def handle_metadata(sender, **kwargs):
#     # (self, instance, *args, **kwargs):
#     print '\nsend message here\n|||||||||||||||||'
#     if created:
#         print '\nsend message here\n|||||||||||||||||'

        
# class MsgBro(Msg):
#     class Meta:
#         proxy = True
#         
#     @classmethod    
#     def evaluate_template(cls, text, context, org=None, url_encode=False, partial_vars=False):
#         """
#         Given input ```text```, tries to find variables in the format @foo.bar and replace them according to
#         the passed in context, contact and org. If some variables are not resolved to values, then the variable
#         name will remain (ie, @foo.bar).
# 
#         Returns a tuple of the substituted text and whether there were are substitution failures.
#         """
#         return 'Fuck you'
#     
#     @classmethod    
#     def fuckyou(cls):
#         return 'nyonko'   

# @classmethod    
# def evaluate_template(cls, text, context, org=None, url_encode=False, partial_vars=False):
#     """
#     Given input ```text```, tries to find variables in the format @foo.bar and replace them according to
#     the passed in context, contact and org. If some variables are not resolved to values, then the variable
#     name will remain (ie, @foo.bar).
# 
#     Returns a tuple of the substituted text and whether there were are substitution failures.
#     """
# 
#     return 'Fuck you'     


 
# def get_myname(cls):
#     return "skinny bitch as"
# #temba.msgs.models.Msg
# Msg.fuck = classmethod(get_myname)
#Msg.add_to_class("evaluate_template",evaluate_template)
       
#         logger.debug('Unable to get currency for channel countries.')
#         logger.info('Unable to get currency for channel countries.')
#         print '\n====================redady to alter this motherfaka\n======================='         
# #         return super(MsgProxy, cls).evaluate_template(cls, text, context, org=None, url_encode=False, partial_vars=False)
#        
        # shortcut for cases where there is no way we would substitute anything as there are no variables
#         if not text or text.find('@') < 0:
#             return text, []
# 
#         # add 'step.contact' if it isn't populated for backwards compatibility
#         if 'step' not in context:
#             context['step'] = dict()
#         if 'contact' not in context['step']:
#             context['step']['contact'] = context.get('contact')
# 
#         if not org:
#             dayfirst = True
#             tz = timezone.get_current_timezone()
#         else:
#             dayfirst = org.get_dayfirst()
#             tz = org.timezone
# 
#         (format_date, format_time) = get_datetime_format(dayfirst)
# 
#         now = timezone.now().astimezone(tz)
# 
#         # add date.* constants to context
#         context['date'] = {
#             '__default__': now.isoformat(),
#             'now': now.isoformat(),
#             'today': datetime_to_str(timezone.now(), format=format_date, tz=tz),
#             'tomorrow': datetime_to_str(timezone.now() + timedelta(days=1), format=format_date, tz=tz),
#             'yesterday': datetime_to_str(timezone.now() - timedelta(days=1), format=format_date, tz=tz)
#         }
# 
#         date_style = DateStyle.DAY_FIRST if dayfirst else DateStyle.MONTH_FIRST
#         context = EvaluationContext(context, tz, date_style)
# 
#         # returns tuple of output and errors
#         return evaluate_template(text, context, url_encode, partial_vars)
