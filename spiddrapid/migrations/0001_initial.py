# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-06-20 13:31
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('locations', '0010_adminboundary_path'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128, verbose_name='Name')),
                ('plan', models.CharField(choices=[('FREE', 'Free Plan'), ('TRIAL', 'Trial'), ('TIER_39', 'Bronze'), ('TIER1', 'Silver'), ('TIER2', 'Gold (Legacy)'), ('TIER3', 'Platinum (Legacy)'), ('TIER_249', 'Gold'), ('TIER_449', 'Platinum')], default='FREE', help_text='What plan your organization is on', max_length=16, verbose_name='Plan')),
                ('plan_start', models.DateTimeField(auto_now_add=True, help_text='When the user switched to this plan', verbose_name='Plan Start')),
                ('language', models.CharField(blank=True, choices=[('en-us', 'English'), ('pt-br', 'Portuguese'), ('fr', 'French'), ('es', 'Spanish')], help_text='The main language used by this organization', max_length=64, null=True, verbose_name='Language')),
                ('config', models.TextField(help_text='More Organization specific configuration', null=True, verbose_name='Configuration')),
                ('slug', models.SlugField(blank=True, error_messages={b'unique': 'This slug is not available'}, max_length=255, null=True, unique=True, verbose_name='Slug')),
                ('administrators', models.ManyToManyField(help_text='The administrators in your organization', related_name='customer_admins', to=settings.AUTH_USER_MODEL, verbose_name='Administrators')),
                ('country', models.ForeignKey(blank=True, help_text='The country this organization should map results for.', null=True, on_delete=django.db.models.deletion.SET_NULL, to='locations.AdminBoundary')),
            ],
        ),
        migrations.CreateModel(
            name='ResultByContact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('contactid', models.IntegerField(default=0, help_text='Contact')),
                ('orgid', models.IntegerField(default=0, help_text='Organisation')),
                ('lastflowrunid', models.IntegerField(default=1, help_text='Last flow Run')),
                ('frequency', models.IntegerField(default=0, help_text='Frequency')),
                ('fieldkey', models.TextField(blank=True, default='', help_text='Fields name', null=True)),
                ('fieldvalue', models.TextField(blank=True, default='', help_text='Fields value', null=True)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='resultbycontact',
            unique_together=set([('contactid', 'fieldkey', 'fieldvalue')]),
        ),
    ]
