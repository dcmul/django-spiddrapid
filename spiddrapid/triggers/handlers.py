from __future__ import unicode_literals

from temba.msgs.handler import MessageHandler
from temba.triggers.models import Trigger
from temba.msgs.models import Msg
from temba.flows.models import Flow, FlowRun, FlowStart,FlowStep
import requests
# import pawulo
import time
import json
from django.conf import settings


class NLUHandler(MessageHandler):
    def __init__(self):
        super(NLUHandler, self).__init__('triggers')

    def handle(self, msg):
        return self.nlu_triggers(msg, Trigger.TYPE_CATCH_ALL, msg.channel)

    def nlu_triggers(self, entity, trigger_type, channel, referrer_id=None, extra=None):
        
        print('\n==\nhrtrt\n==\n')
        # return False 

        if not isinstance(entity, Msg):
            return False
                                
        contact = entity.contact
        start_msg = entity


        steps = FlowStep.get_active_steps_for_contact(contact)
        print("\n++++flow steps++++\n")
        if steps:
            print("\n=send error=\n")
            extra_error = extra if extra else {}
            extra_error.update({'error': True, 'message': start_msg.text })
            return Trigger.catch_triggers(entity, Trigger.TYPE_CATCH_ALL, entity.channel, referrer_id, extra_error)       
        # print(steps)

        

        # print(contact)
        # print(start_msg)
        url = '%s/parse' % (settings.RASA_URL, )
        data  =  {
            'project':'insta_project',
            'q': start_msg.text 
            }

        headers = {
            'Content-Type': 'application/json',
            }

        response = requests.post(url, data=json.dumps(data), headers=headers)
        

        print(response)
        print(response.text)
        r = json.loads(response.text)

        if r['intent']['confidence'] < settings.SPIDDRAPID_CONFIDENCE_LEVEL3:
            # Level 4
            print('=====level4=====')
            return False


        if r['intent']['confidence'] < settings.SPIDDRAPID_CONFIDENCE_LEVEL2:
            # Level 3
            print('=====level3=====')
            return False
        
        intent_array = r['intent']['name'].split('__')
        print(intent_array)
        flow_name = intent_array[0].replace('_',' ')
        
        # flow_name = r['intent']['name'].replace('_',' ')
        flow = Flow.objects.filter(name__iexact=flow_name).first()
        # print(flow.query)
        # print('==before flow==')
        if not flow:
            print('==flow missing==')
            return False

        extra =  { 'nlu': r['entities'], "confirmed": True  }

        if r['intent']['confidence'] < settings.SPIDDRAPID_CONFIDENCE_LEVEL1:
            # Level 2 add to extra
            print('=====level2=====')
            extra =  { 'nlu': r['entities'] , "confirmed": False }
            # return False                
        extra.update({'data': { i['entity']:i['value'] for i in r['entities'] }})  
        if len(intent_array) > 1:
            extra.update({'cmdvalue': intent_array[1]})    
                   
        print('=returned=')
        print(extra)
        flow.start([], [contact], start_msg=start_msg, restart_participants=True, extra=extra)


        return True




