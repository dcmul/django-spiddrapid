from django.conf.urls import url
from django.views.generic import TemplateView

from temba.channels.handlers import get_channel_handlers
from temba.urls import * 

from spiddrapid.channels.handlers import PawuloHandler
from spiddrapid.channels.handlers import FacebookHandler
from spiddrapid.channels.handlers import TelegramHandler
from spiddrapid.channels.handlers import KarixHandler


courier_urls = []
handler_urls = []

for handler in [FacebookHandler, PawuloHandler, TelegramHandler, KarixHandler]:
#     print(handler)
#     print('-')
    rel_url, url_name = handler.get_courier_url()
    if rel_url:
        courier_urls.append(url(rel_url, handler.as_view(), name=url_name))

    rel_url, url_name = handler.get_handler_url()
    if rel_url:
        handler_urls.append(url(rel_url, handler.as_view(), name=url_name))


# print(courier_urls)
# print(handler_urls)

urlpatterns = [
    url(r'^c/', include(courier_urls)),
    url(r'^handlers/', include(handler_urls)),
    # url(r'^$', views.index, name='index'),
    # url(r'^api/v1/stripe/$', StripeHandler.as_view()) 
    # url(r'^test/$', TemplateView.as_view(template_name="spiddrapid/test.html")),       
] + urlpatterns


