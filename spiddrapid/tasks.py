'''
Created on Feb 26, 2018

@author: dan

'''
from __future__ import unicode_literals

import logging
import six
from spiddrapid.utils.general import list_order_by_freq
from temba.flows.models import ExportFlowResultsTask, Flow, FlowStart, FlowRun, FlowStep
from celery.task import task
from celery import shared_task
from spiddrapid.models import ResultByContact
from temba.contacts.models import ContactField
import json
import requests
from django.conf import settings
from temba.msgs.models import Msg
from temba.flows.models import Flow, FlowRun, FlowStart,FlowStep
from temba.utils import format_decimal, get_dict_from_cursor, dict_to_json, json_to_dict

@task( name="update_contact_fields_from_flow_task")
def update_contact_fields_from_flow():
    print "amre here"
    lastid = ResultByContact.get_lastrunid()
    #runs = FlowRun.objects.filter(id__gt=lastid, is_active=False).order_by('id')
    runs = FlowRun.objects.all()
#     runs = FlowRun.objects.filter() #TODO filter properly, get for last 30 minutes
#     contactruns = FlowRun.objects.only('contact').distinct('contact') #TODO filter properly, get for last 30 minutes
    print  runs.count()
    for run in runs:
        print "id  is %d" % run.id
        if not run.results:
            continue
        contact = run.contact 
        results = json_to_dict(run.results)
        
        for key, value in  results.iteritems():
            # print key, value
            contactfield = ContactField.get_by_label(run.org, value['name'])
            print "^^^ %s" % run.org.id
            if not contactfield:
                continue
            print "\n~~~~~label %s exists~~~~~~`\n" % value['name']
            obj, created = ResultByContact.objects.get_or_create(
                contactid=contact.id,
                fieldkey=key,
                fieldvalue=value['value'].lower(),
                defaults={'orgid': run.org.id},
            )
            obj.frequency += 1
            obj.lastflowrunid = run.id
            obj.save()
            
            # do an analysis
            x = ResultByContact.objects.filter(
                    contactid=contact.id,
                    fieldkey=contactfield.key,
                ).order_by('-frequency')[:100].values_list('fieldvalue', flat=True)
            # print 
            # print "the json %s " % json.dumps(list(x)) # convert tuple to list
            wordlist = json.dumps(list(x)) # convert tuple to list
            contact.set_field(contact.org.administrators.first(), contactfield.key, wordlist)
            
            # print obj.frequency
            # print obj.lastflowrunid
            
            # print 'created' if created else 'Not created'

            print "-------------get field %s " % contact.get_field(contactfield.key).string_value
#         if ResultByContact.objects.filter(contactid=run.contact.id, fieldname=False, fieldvalue="").exists():
# #         add record
#             pass
#             
#         else :
#             pass
# #         update record     
#         contact = run.contact
#         runs = FlowRun.objects.filter(contact=contact)
#         for run in runs:
#             print run.results
# #         print contactrun.contact
    
#     pass
@shared_task
def test_kiyembe():
    print "\n________\nfrom text\n________\n"
    return 'am here send me'


@shared_task(track_started=True, name="spiddrapid.tasks.add")
def add(x, y):
    return x + y

@shared_task(track_started=True, name="spiddrapid.tasks.startcommand")
def startcommand(channel, contact, text):
    print("\n~~~~~~\ncommand\n~~~~~~~\n")
    text = text.replace('_', ' ')
    print(text)
    print('\n==\nhrtrt\n==\n')
    # return False 

    # if not isinstance(entity, Msg):
    #     return False
                            
    # contact = entity.contact
    # start_msg = entity


    steps = FlowStep.get_active_steps_for_contact(contact)
    print("\n++++flow steps++++\n")
    if steps:
        print("\n=send error=\n")
        # extra_error = extra if extra else {}
        # extra_error.update({'error': True, 'message': start_msg.text })
        # return Trigger.catch_triggers(entity, Trigger.TYPE_CATCH_ALL, entity.channel, referrer_id, extra_error)       

        # return False
    
    print(steps)

    

    print(contact)
    # print(start_msg)
    url = '%s/parse' % (settings.RASA_URL, )
    data  =  {
        'project':'insta_project',
        'q': text
        }

    headers = {
        'Content-Type': 'application/json',
        }

    response = requests.post(url, data=json.dumps(data), headers=headers)
    

    print(response)
    print(response.text)
    r = json.loads(response.text)

    if r['intent']['confidence'] < settings.SPIDDRAPID_CONFIDENCE_LEVEL3:
        # Level 4
        print('=====level4=====')
        return False


    if r['intent']['confidence'] < settings.SPIDDRAPID_CONFIDENCE_LEVEL2:
        # Level 3
        print('=====level3=====')
        return False
    

    intent_array = r['intent']['name'].split('__')
    print(intent_array)
    flow_name = intent_array[0].replace('_',' ')
    flow = Flow.objects.filter(name__iexact=flow_name).first()
    # print(flow.query)
    if not flow:
        print('=flow doesnt exist=')
        return False

    extra =  { 'nlu': r['entities'], "confirmed": True  }

    if r['intent']['confidence'] < settings.SPIDDRAPID_CONFIDENCE_LEVEL1:
        # Level 2 add to extra
        print('=====level2=====')
        extra =  { 'nlu': r['entities'] , "confirmed": False }
        # return False                
    extra.update({'data': { i['entity']:i['value'] for i in r['entities'] }}) 
     
    if len(intent_array) > 1:
        extra.update({'cmdvalue': intent_array[1]})     
    print('=returned=')
    print(extra)
    flow.start([], [contact],  restart_participants=True, extra=extra)


    return True


@shared_task(track_started=True, name="spiddrapid.tasks.typing")
def typing(channel_type, contact_path, channel_config):
    print('\n--\nuser typing\n--\n')
    # print(msg)
    return True   