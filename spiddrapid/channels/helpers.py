'''
Created on Feb 25, 2018

@author: dan
'''
from temba.contacts.models import Contact
from __builtin__ import setattr
from temba_expressions.functions.excel import false

class ChannelActionMixin(object):
    '''
    classdocs
    '''
    
    spidd_all_tags = [
        'spidd_replies',
        'spidd_buttons',
        'spidd_template',
        'spidd_keyboard',
        'spidd_typing', 
        'spidd_receipts ',         
        ]
    
    spidd_allowed_tags = []
    
    
    def handle_rich_messages(self,channel, msg, text):
        import json
        # print "\n=88=\n {# #}\n=88=\n"      
        #metadata = msg.metadata if hasattr(msg, 'metadata') else {}
        #quick_replies = metadata.get('quick_replies', []) 
        print "==quick replies=="
        print msg.contact
     
#         try:
#             metadict = json.loads(msg.metadata)
#         except ValueError, e:
#             return False  
        metadict = msg.metadata if hasattr(msg, 'metadata') else None
            
        if not metadict:
            return False
        
        
        activetags = [tag for tag in self.get_allowed_rich_tags() if tag in metadict]
        
        if not activetags:
            return False
        
        
        for activetag in activetags:
            tag_function = getattr(self,"handle_%s" % (activetag),self.func_not_found) 
            stop_exec = tag_function(channel, msg, text)
            if stop_exec:
                return True
    

        print "text1 %s" % text
        return False    
        
#         for tag in get_richtext_tags() if tag in meta_dict:
#             print 'this tag' 07
#             break
        
                     

#         if json_str['cmd'] !='quick_replies':
#         	return (False, text)  # Handle other commands and dont 
# 
#         if 'fieldname' in json_str and hasattr(msg, 'metadata'):
#             contact = Contact.objects.get(pk=msg.contact)
#             print '==print contact=='
#             print contact
#             replyfield = contact.get_field(json_str['fieldname'])
#             #quick_replies = msg.metadata['quick_replies']
#             quick_replies = replyfield.string_value if type(replyfield) is list else []
# #             if not hasattr(msg, 'metadata'):
# # #                 msg.metadata = {}
# #                 setattr(msg,'metadata', {})
#             msg.metadata.update({'quick_replies':quick_replies})   	 
#        	
        # #chech markup
        # msg.contact.get_field(contactfield.key).string_value     

    
    def func_not_found(self,channel, msg, text):
        print '\n == herre is uselsse ==\n'
        
        return False
    
    def handle_spidd_replies(self,channel, msg, text):
        import json
        print '\n == herre is uselsse ==\n'
        json_str = msg.metadata['spidd_replies']
        if 'fieldname' in json_str:
            contact = Contact.objects.get(pk=msg.contact)
            print '==print contact=='
            print contact
            replyfield = contact.get_field(json_str['fieldname'])
            #quick_replies = msg.metadata['quick_replies']
            if not replyfield:
                return False
            
            if not replyfield.string_value:
                return False
            
            quick_replies = json.loads(replyfield.string_value)
#             if not hasattr(msg, 'metadata'):
# #                 msg.metadata = {}
#                 setattr(msg,'metadata', {})
            msg.metadata.update({'quick_replies':quick_replies})               
        return False    
#     @staticmethod
    def get_allowed_rich_tags(self):
        return self.spidd_allowed_tags

      