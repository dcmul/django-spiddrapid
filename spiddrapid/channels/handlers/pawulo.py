from __future__ import absolute_import, unicode_literals

import hmac
import hashlib
import iso8601
import json
import pytz
import requests
import six
import magic
import xml.etree.ElementTree as ET
import logging

from datetime import datetime
from django.conf import settings
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.utils import timezone
from django.utils.crypto import constant_time_compare
from django.utils.dateparse import parse_datetime
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django_redis import get_redis_connection
from requests import Request

from temba.api.models import WebHookEvent
from temba.channels.models import Channel, ChannelLog, ChannelEvent
from temba.contacts.models import Contact, URN
# from temba.flows.models import Flow, FlowRun, FlowStep
# from temba.orgs.models import NEXMO_UUID
from temba.msgs.models import Msg, HANDLE_EVENT_TASK, HANDLER_QUEUE, MSG_EVENT, OUTGOING
from temba.triggers.models import Trigger
from temba.ussd.models import USSDSession
from temba.utils import get_anonymous_user, on_transaction_commit
from temba.utils.dates import json_date_to_datetime, ms_to_datetime
from temba.utils.queues import push_task
from temba.utils.http import HttpEvent
# from temba.utils.jiochat import JiochatClient
from temba.utils.text import decode_base64
# from temba.utils.twitter import generate_twitter_signature
# from twilio import twiml
# from .tasks import fb_channel_subscribe, refresh_jiochat_access_tokens

from temba.channels.handlers import BaseChannelHandler
# from temba.channels.models import Channel, ChannelLog, ChannelEvent
# from django.http import HttpResponse, JsonResponse
from spiddrapid.tasks import startcommand




class PawuloHandler(BaseChannelHandler):
    courier_url = r'^pw/(?P<uuid>[a-z0-9\-]+)/receive$'
    courier_name = 'courier.pw'

    handler_url = r'^pawulo/(?P<uuid>[a-z0-9\-]+)/?$'
    handler_name = 'handlers.pawulo_handler'

    @classmethod
    def download_file(cls, channel, file_id):
        """
        Fetches a file from Telegram's server based on their file id
        """
        auth_token = channel.config_json()[Channel.CONFIG_AUTH_TOKEN]
        url = 'https://api.telegram.org/bot%s/getFile' % auth_token
        response = requests.post(url, {'file_id': file_id})

        if response.status_code == 200:
            if json:
                response_json = response.json()
                if response_json['ok']:
                    url = 'https://api.telegram.org/file/bot%s/%s' % (auth_token, response_json['result']['file_path'])
                    extension = url.rpartition('.')[2]
                    response = requests.get(url)

                    # attempt to determine our content type using magic bytes
                    content_type = None
                    try:
                        m = magic.Magic(mime=True)
                        content_type = m.from_buffer(response.content)
                    except Exception:  # pragma: no cover
                        pass

                    # fallback on the content type in our response header
                    if not content_type or content_type == 'application/octet-stream':
                        content_type = response.headers['Content-Type']

                    temp = NamedTemporaryFile(delete=True)
                    temp.write(response.content)
                    temp.flush()

                    return '%s:%s' % (content_type, channel.org.save_media(File(temp), extension))

    def post(self, request, *args, **kwargs):

        request_body = request.body
        request_method = request.method
        request_path = request.get_full_path()

        print('\n==In pawulo hander==\n')
        body = json.loads(request.body)
        print(request.body)
        if 'message' in body and  'chat' in body['message']:
            if body['message']['chat'].get('type') == 'command':
                print('=do nlu=')
                print(body['message']['text'])

                channel_uuid = kwargs['uuid']
                channel = Channel.objects.filter(uuid=channel_uuid, is_active=True, channel_type='PW').first()


                pawulo_id = str(body['message']['from']['id'])
                urn = URN.from_external(pawulo_id)
                contact = Contact.from_urn(channel.org, urn)     


                if not contact:
                    # "from": {"id": 25028612,"first_name": "Eric", "last_name": "Newcomer", "username": "ericn" }
                    name = " ".join((body['message']['from'].get('first_name', ''), body['message']['from'].get('last_name', '')))
                    name = name.strip()

                    username = body['message']['from'].get('username', '')
                    if not name and username:  # pragma: needs cover
                        name = username

                    if channel.org.is_anon:
                        name = None

                    contact = Contact.get_or_create(channel.org, channel.created_by, name, urns=[urn])

                print(contact)
                print(channel)
                status = 201
                status_msg = 'command recieved'
                startcommand(channel, contact, body['message']['text'] )
                return JsonResponse(dict(detail=status_msg, status=status))

        def make_response(description, msg=None, status_code=None):
            response_body = dict(description=description)
            if msg:
                log(msg, 'Incoming message', json.dumps(response_body))

            if not status_code:
                status_code = 201
            return JsonResponse(response_body, status=status_code)

        def log(msg, description, response_body):
            event = HttpEvent(request_method, request_path, request_body, 200, response_body)
            ChannelLog.log_message(msg, description, event)

        channel_uuid = kwargs['uuid']
        channel = Channel.objects.filter(uuid=channel_uuid, is_active=True, channel_type='PW').first()

        if not channel:  # pragma: needs cover
            return HttpResponse("Channel with uuid: %s not found." % channel_uuid, status=404)

        body = json.loads(request.body)

        if 'message' not in body:
            return make_response('No "message" found in payload', status_code=400)

        # look up the contact
        pawulo_id = str(body['message']['from']['id'])
        urn = URN.from_external(pawulo_id)
        contact = Contact.from_urn(channel.org, urn)

        # if the contact doesn't exist, try to create one
        if not contact:
            # "from": {
            # "id": 25028612,
            # "first_name": "Eric",
            # "last_name": "Newcomer",
            # "username": "ericn" }
            name = " ".join((body['message']['from'].get('first_name', ''), body['message']['from'].get('last_name', '')))
            name = name.strip()

            username = body['message']['from'].get('username', '')
            if not name and username:  # pragma: needs cover
                name = username

            if channel.org.is_anon:
                name = None

            contact = Contact.get_or_create(channel.org, channel.created_by, name, urns=[urn])

        text = ""
        attachments = []
        msg_date = datetime.utcfromtimestamp(body['message']['date']).replace(tzinfo=pytz.utc)

        def fetch_attachment(media_type):
            attachment = body['message'][media_type]
            if isinstance(attachment, list):
                attachment = attachment[-1]
                if isinstance(attachment, list):  # pragma: needs cover
                    attachment = attachment[0]

            # if we got a media URL for this attachment, save it away
            media_url = PawuloHandler.download_file(channel, attachment['file_id'])
            if media_url:
                attachments.append(media_url)

        if 'text' in body['message']:
            text = body['message']['text']
        elif 'caption' in body['message']:
            text = body['message']['caption']
        elif 'contact' in body['message']:  # pragma: needs cover
            contact_block = body['message']['contact']

            if 'first_name' in contact_block and 'phone_number' in contact_block:
                text = '%(first_name)s (%(phone_number)s)' % contact_block
            elif 'first_name' in contact_block:
                text = '%(first_name)s' % contact_block
            elif 'phone_number' in contact_block:
                text = '%(phone_number)s' % contact_block
        elif 'venue' in body['message']:
            if 'title' in body['message']['venue']:
                text = body['message']['venue']['title']

        for msg_type in ('sticker', 'video', 'voice', 'document', 'photo'):
            if msg_type in body['message']:
                fetch_attachment(msg_type)

        if 'location' in body['message']:
            location = body['message']['location']
            attachments.append('geo:%s,%s' % (location['latitude'], location['longitude']))

        if text.strip() == "/start":
            event = ChannelEvent.create(channel, urn, ChannelEvent.TYPE_NEW_CONVERSATION, timezone.now())
            event.handle()
            return make_response("Conversation started")

        if text or attachments:
            msg = Msg.create_incoming(channel, urn, text, attachments=attachments, date=msg_date)
            log(msg, 'Inbound message', json.dumps(dict(description='Message accepted')))
            return make_response('Message accepted', msg)
        else:
            return make_response("Ignored, nothing provided in payload to create a message")
