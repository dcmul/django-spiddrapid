from __future__ import absolute_import, unicode_literals
import json

from temba.channels.handlers import TelegramHandler as BaseTelegramHandler

from temba.channels.handlers import FacebookHandler as BaseFacebookHandler
from temba.channels.models import Channel, ChannelLog, ChannelEvent
from temba.contacts.models import Contact, URN
from django.http import HttpResponse, JsonResponse

from spiddrapid.tasks import startcommand

class TelegramHandler(BaseTelegramHandler):

    def post(self, request, *args, **kwargs):
        print('\n==In telegram hander==\n')
        print(request.body)
        body = json.loads(request.body)

        if 'message' in body and 'entities' in body['message']:
            print('=nlu=')
            for entity in  body['message']['entities']:
                text = body['message']['text']
                if entity['type'] == 'bot_command' and text.strip() != '/start' :
                    print('\n==in arrayy==n')
                    print(text.split(' ')[1] )
                    text_id = text.split(' ')[1]

                    channel_uuid = kwargs['uuid']
                    channel = Channel.objects.filter(uuid=channel_uuid, is_active=True, channel_type='TG').first()
                    
                    telegram_id = str(body['message']['from']['id'])
                    urn = URN.from_telegram(telegram_id)
                    contact = Contact.from_urn(channel.org, urn)
                    
                    if not contact:
                        # "from": {"id": 25028612, "first_name": "Eric", "last_name": "Newcomer", "username": "ericn" }
                        name = " ".join((body['message']['from'].get('first_name', ''), body['message']['from'].get('last_name', '')))
                        name = name.strip()

                        username = body['message']['from'].get('username', '')
                        if not name and username:  # pragma: needs cover
                            name = username

                        if channel.org.is_anon:
                            name = None

                        contact = Contact.get_or_create(channel.org, channel.created_by, name, urns=[urn])
                    
                    print(channel)
                    print(contact)
                    status = 201
                    status_msg = 'command recieved'
                    startcommand(channel, contact, text_id )
                    return JsonResponse(dict(detail=status_msg, status=status))                    

        return super(TelegramHandler, self).post(request, *args, **kwargs)
    
