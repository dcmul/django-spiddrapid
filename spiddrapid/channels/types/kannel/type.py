from __future__ import unicode_literals, absolute_import

import time

# import phonenumbers
# import requests
# import six
# from django.urls import reverse
# from django.utils.http import urlencode
# 
# from django.utils.translation import ugettext_lazy as _
# 
# from temba.channels.types.kannel.views import ClaimView
# from temba.contacts.models import TEL_SCHEME
# from temba.msgs.models import WIRED
# from temba.utils.http import HttpEvent
from ...helpers import ChannelActionMixin
from temba.channels.types.kannel import KannelType as BaseKannelType
from temba.channels.models import Channel, ChannelType, SendException, Encoding


class KannelType(ChannelActionMixin, BaseKannelType):
    spidd_allowed_tags = [
        'spidd_replies',
        'spidd_buttons',
        'spidd_template',
        'spidd_keyboard',
        'spidd_typing', 
        'spidd_receipts ',         
        ]
    def send(self, channel, msg, text):
        # build our callback dlr url, kannel will call this when our message is sent or delivered
        print "====testing====\n%s\n" % (text)
#       text = "%s\n1.item1\n2.item2" % (text)
        handle_rich = self.handle_rich_messages(channel, msg, text)
        if handle_rich:
            return

        metadata = msg.metadata if hasattr(msg, 'metadata') else {}
        quick_replies = metadata.get('quick_replies', [])
        # print quick_replies
        text += ""
        for key, quick_reply in enumerate(quick_replies):
            text += "\n%d. %s" % (key+1, quick_reply)
        # print "\n=====%s\n======" % (text)          
        super(KannelType, self).send(channel, msg, text)
