from __future__ import unicode_literals, absolute_import

import json
import requests
import six
import time

from django.utils.translation import ugettext_lazy as _
from temba.contacts.models import Contact, ContactURN, URN, FACEBOOK_SCHEME
from temba.msgs.models import Attachment, WIRED
from temba.orgs.models import Org
from temba.triggers.models import Trigger
from temba.channels.types.facebook import FacebookType as BaseFacebookType
from temba.utils.http import HttpEvent
from .views import ClaimView
from temba.channels.models import Channel, ChannelType, SendException


class FacebookType(BaseFacebookType):
    """
    A Facebook channel
    """
    def send_typing(self, channel, msg):
        print('\n-inside facebook typing-\n')
        print(msg.contact_urn.path)
        print('\n-inside facebook typing-\n')

        payload = {'sender_action':'typing_on'}
        url = "https://graph.facebook.com/v2.5/me/messages"
        params = {'access_token': json.loads(channel.config)[Channel.CONFIG_AUTH_TOKEN]}
        headers = {'Content-Type': 'application/json'}
        # start = time.time()
        if URN.is_path_fb_ref(msg.contact_urn.path):
            payload['recipient'] = dict(user_ref=URN.fb_ref_from_path(msg.contact_urn.path))
        else:
            payload['recipient'] = dict(id=msg.contact_urn.path)
        payload = json.dumps(payload)

        print(payload)

        try:
            response = requests.post(url, payload, params=params, headers=headers, timeout=15)
            print(response.text)
        except Exception as e:
            print('error occured')
        

