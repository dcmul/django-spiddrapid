from __future__ import unicode_literals, absolute_import

import requests
# import pawulo
import time
import json

from django.urls import reverse
from django.utils.http import urlencode
from django.utils.translation import ugettext_lazy as _
from temba.contacts.models import EXTERNAL_SCHEME
from temba.msgs.models import Attachment, WIRED
from temba.utils.http import HttpEvent
from .views import ClaimView
# from ...models import Channel, ChannelType, SendException
from temba.channels.models import Channel, ChannelType, SendException



class PawuloType(ChannelType):
    """
    A Pawulo bot channel
    """
    code = 'PW'
    category = ChannelType.Category.SOCIAL_MEDIA

    name = "Pawulo"
    icon = 'icon-pawulo'
    show_config_page = False

    claim_blurb = _("""Add a <a href="https://pawulo.com">pawulo</a> bot to send and receive messages to pawulo
    users for free. We can send messages through.""")
    claim_view = ClaimView

    # schemes = [PAWULO_SCHEME]
    schemes = [EXTERNAL_SCHEME]
    max_length = 1600
    attachment_support = True
    free_sending = True

    def activate(self, channel):
        config = channel.config_json()
        # bot = pawulo.Bot(config['auth_token'])
        # bot.set_webhook("https://" + channel.callback_domain + reverse('courier.tg', args=[channel.uuid]))

    def deactivate(self, channel):
        config = channel.config_json()
        # bot = pawulo.Bot(config['auth_token'])
        # bot.delete_webhook()

    def send(self, channel, msg, text):
        auth_token = channel.config['auth_token']
        url_prefix = channel.config['send_url']
        print('\n=val=\n')
        print(channel.config)
        # auth_token = 'myauth'
        # domain = 'soc.pawulo.com'
        # send_url = 'https://www.pawulo.com/bot%s/sendMessage' % auth_token
        send_url = '%s/sendMessage' % url_prefix
        post_body = {'chat_id': msg.urn_path, 'text': text}

        metadata = msg.metadata if hasattr(msg, 'metadata') else {}
        quick_replies = metadata.get('quick_replies', [])
        formatted_replies = json.dumps(dict(resize_keyboard=True, one_time_keyboard=True,
                                            keyboard=[[dict(text=item[:self.quick_reply_text_size])] for item in quick_replies]))

        if quick_replies:
            post_body['reply_markup'] = formatted_replies

        start = time.time()

        # for now we only support sending one attachment per message but this could change in future
        attachments = Attachment.parse_all(msg.attachments)
        attachment = attachments[0] if attachments else None

        if attachment:
            category = attachment.content_type.split('/')[0]
            if category == 'image':
                send_url = '%s/sendPhoto' % url_prefix
                post_body['photo'] = attachment.url
                post_body['caption'] = text
                del post_body['text']
            elif category == 'video':
                send_url = '%s/sendVideo' % url_prefix
                post_body['video'] = attachment.url
                post_body['caption'] = text
                del post_body['text']
            elif category == 'audio':
                send_url = '%s/sendAudio' % url_prefix
                post_body['audio'] = attachment.url
                post_body['caption'] = text
                del post_body['text']
        
        print(send_url)
        params = {}
        payload = json.dumps(post_body)
        headers = {'Content-Type': 'application/json', 'Authorization':'Token %s' % (auth_token, )}
        event = HttpEvent('POST', send_url, json.dumps(payload))

        try:
            # response = requests.post(send_url, post_body)
            response = requests.post(send_url, payload, params=params, headers=headers, timeout=15)

            event.status_code = response.status_code
            event.response_body = response.text
            
            print('\n==%s==\n' % response.json()['result'])

            external_id = response.json()['result']['message_id']
        except Exception as e:
            raise SendException(str(e), event=event, start=start)

        Channel.success(channel, msg, WIRED, start, event=event, external_id=external_id)
