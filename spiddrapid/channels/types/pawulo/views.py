from __future__ import unicode_literals, absolute_import

# import pawulo

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from smartmin.views import SmartFormView
from temba.channels.models import Channel
from temba.channels.views import ClaimViewMixin


class ClaimView(ClaimViewMixin, SmartFormView):
    class Form(ClaimViewMixin.Form):

        auth_token = forms.CharField(label=_("Authentication Token"),
                                     help_text=_("The Authentication token for your pawulo Bot"))
        
        # scheme = forms.ChoiceField(choices=ContactURN.SCHEME_CHOICES, label=_("URN Type"),
        #                            help_text=_("The type of URNs handled by this channel"))

        number = forms.CharField(max_length=14, min_length=1, label=_("Number"), required=False,
                                 help_text=_("xThe phone number or that this channel will send from"))

        # handle = forms.CharField(max_length=32, min_length=1, label=_("Handle"), required=False,
        #                          help_text=_("The Twitter handle that this channel will send from"))

        address = forms.CharField(max_length=64, min_length=1, label=_("Address"), required=False,
                                  help_text=_("The external address that this channel will send from"))

        # country = forms.ChoiceField(choices=ALL_COUNTRIES, label=_("Country"), required=False,
        #                             help_text=_("The country this phone number is used in"))

        # method = forms.ChoiceField(choices=(('POST', "HTTP POST"), ('GET', "HTTP GET"), ('PUT', "HTTP PUT")),
        #                            help_text=_("What HTTP method to use when calling the URL"))

        # content_type = forms.ChoiceField(choices=Channel.CONTENT_TYPE_CHOICES,
        #                                  help_text=_("The content type used when sending the request"))

        # max_length = forms.IntegerField(initial=160, validators=[MaxValueValidator(640), MinValueValidator(60)],
        #                                 help_text=_("The maximum length of any single message on this channel. "
        #                                             "(longer messages will be split)"))

        url = forms.URLField(max_length=1024, label=_("Send URL"),
                             help_text=_("The URL we will call when sending messages. (Dont end URL with a forward slash)"))

        # body = forms.CharField(max_length=2048, label=_("Request Body"), required=False, widget=forms.Textarea,
        #                        help_text=_(
        #                            "The request body if any, with variable substitutions (only used for PUT or POST)"))

        def clean_auth_token(self):
            org = self.request.user.get_org()
            value = self.cleaned_data['auth_token']

            # does a bot already exist on this account with that auth token
            for channel in Channel.objects.filter(org=org, is_active=True, channel_type=self.channel_type.code):
                if channel.config_json()['auth_token'] == value:
                    raise ValidationError(_("A pawulo channel for this bot already exists on your account."))

            # try:
            #     bot = pawulo.Bot(token=value)
            #     bot.get_me()
            # except pawulo.PawuloError:
            #     raise ValidationError(_("Your authentication token is invalid, please check and try again"))

            return value

    form_class = Form

    def form_valid(self, form):
        org = self.request.user.get_org()
        auth_token = self.form.cleaned_data['auth_token']
        # org = self.request.user.get_org()
        data = form.cleaned_data        

        # bot = pawulo.Bot(auth_token)
        # me = bot.get_me()


        channel_config = {
            Channel.CONFIG_SEND_URL: data['url'],
            Channel.CONFIG_SEND_METHOD: data['number'],
            Channel.CONFIG_SEND_BODY: data['address'],
            Channel.CONFIG_AUTH_TOKEN: auth_token,
            Channel.CONFIG_CALLBACK_DOMAIN: org.get_brand_domain()
            # Channel.CONFIG_CONTENT_TYPE: data['content_type'],
            # Channel.CONFIG_MAX_LENGTH: data['max_length']
        }

        # channel_config = {Channel.CONFIG_AUTH_TOKEN: auth_token, Channel.CONFIG_CALLBACK_DOMAIN: org.get_brand_domain()}

        self.object = Channel.create(org, self.request.user, None, self.channel_type,
                                     name='pawulopw', address='', config=channel_config)

        return super(ClaimView, self).form_valid(form)
