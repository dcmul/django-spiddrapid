from django.contrib.staticfiles.storage import staticfiles_storage
from django.urls import reverse
from jinja2 import Environment

from spiddrapid.utils.jinjaglobals import get_contacts, fromjson, phone_format, format_text

def environment(**options):
    env = Environment(**options)
    env.globals.update({
        'static': staticfiles_storage.url,
        'url': reverse,
        'get_contacts': get_contacts,
        'fromjson' : fromjson,
        'phoneformat': phone_format,
        'formatxt': format_text
    })
    return env