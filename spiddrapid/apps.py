# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig
#import monkey_patching
import temba

class SpiddrapidConfig(AppConfig):
    name = 'spiddrapid'
    def ready(self): 
        import spiddrapid.signals

        @classmethod
        def evaluate_template(cls, text, context, org=None, url_encode=False, partial_vars=False):
            from django.utils import timezone
            # from temba.utils import get_datetime_format, datetime_to_str
            from temba.utils.dates import get_datetime_format, datetime_to_str, datetime_to_s
            from datetime import datetime, timedelta
            from temba_expressions.evaluator import EvaluationContext, DateStyle
            from temba.utils.expressions import evaluate_template
            from .utils.display import render_jinja2_string, render_jinja2_file  
            import json    

            # print '====================results of jinja2 ===================='
            # print text
            # print '============'
#             print json.dumps(context['extra'])
#             print '====================results of jinja2 ===================='


#             if not text or text.find('@') < 0:
#                 return text, []


        # add 'step.contact' if it isn't populated for backwards compatibility
            if 'step' not in context:
                context['step'] = dict()
            if 'contact' not in context['step']:
                context['step']['contact'] = context.get('contact')

            if not org:
                dayfirst = True
                tz = timezone.get_current_timezone()
            else:
                dayfirst = org.get_dayfirst()
                tz = org.timezone

            (format_date, format_time) = get_datetime_format(dayfirst)

            now = timezone.now().astimezone(tz)

        # add date.* constants to context
            context['date'] = {
                '__default__': now.isoformat(),
                'now': now.isoformat(),
                'today': datetime_to_str(timezone.now(), format=format_date, tz=tz),
                'tomorrow': datetime_to_str(timezone.now() + timedelta(days=1), format=format_date, tz=tz),
                'yesterday': datetime_to_str(timezone.now() - timedelta(days=1), format=format_date, tz=tz)
            }

            # for x,y in context.iteritems():
            #     print x
            #     print y
            #     print 'x=================================xd'
            # print partial_vars            
            

            date_style = DateStyle.DAY_FIRST if dayfirst else DateStyle.MONTH_FIRST


            oldcontext = context

            context = EvaluationContext(context, tz, date_style)

            print context

            #context
            # text = render_jinja2_string(text, context)      

            text, error = evaluate_template(text, context, url_encode, partial_vars)


            text = render_jinja2_string(text, oldcontext)
            # returns tuple of output and errors
            return text, error
            

            	# return evaluator.evaluate_template(template, context, url_encode, strategy)
        temba.msgs.models.Msg.add_to_class("evaluate_template",evaluate_template)	

        # global temba
        # temba.utils.expressions.evaluate_template = evaluate_template    
        # print type(temba.utils.expressions.evaluate_template)
