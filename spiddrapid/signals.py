
from temba.msgs.models import Msg
from temba.msgs.models import INCOMING, OUTGOING
from temba.channels.models import Channel
from smartmin.models import SmartModel
from django.dispatch import receiver
from django.db.models.signals import post_save
import json

from spiddrapid.utils.general import extract_richtext, handle_spidd_replies
import logging
from spiddrapid.tasks import typing
logger = logging.getLogger(__name__)

@receiver(post_save, sender=Msg)
def handle_metadata(sender, instance=None, created=False, **kwargs):
    #import json
    # (self, instance, *args, **kwargs):
    # print '\nsend message here\n~~~~~~~~~~~~~~~~~~~~~~~'
    if created:
        logger.warn("\n-=-=-Warning-=-=-\n")

        print '\nMessage created\n|||||||||||||||||'
        # print instance.text

        print "connection%s \nid %d \n %s" % (instance.connection, instance.id, instance.text.encode('utf-8'), )
        print "reply_to %s" % instance.response_to

        # print type(instance.channel)
        # msg = instance
        # metadata = msg.metadata if hasattr(msg, 'metadata') else {}
        if instance.direction == INCOMING:
            print 'incoming'
            alter_incoming(instance)
            if(instance.channel):
                send_typing(instance)
                print('\n--channel--\n: %s', (instance.channel))
                print('\n--channel_type--\n: %s', (instance.channel.channel_type,))
                typing.delay(instance.channel.channel_type, instance.contact_urn.path,  instance.channel.config)


            # print('\n-channel-\n')
            # channel = Channel.get_cached_channel(msg.channel)
            # print(instance.channel)
        elif instance.direction == OUTGOING:
            alter_outgoing(instance) 
            print 'outgoing'
        else:
            pass


        #print metadata, text    
def send_typing(msg=None):
    if not msg.channel:
        return

    print('\n-channel-\n')
    # print(msg)
    # print(msg.channel)
    channel = msg.channel
    channel_type = Channel.get_type_from_code(channel.channel_type)
    # print(channel_type)
    # print(type(channel_type))
    # print('\n-----\n')

    allowed_channels = ['Telegram', 'Facebook']
    if channel_type.name in allowed_channels:
        channel_type.send_typing(channel, msg)

    # channel = Channel.get_cached_channel(msg.channel)  
    # print(instance.channel)  

def alter_outgoing(msg=None):
    metadata, text = extract_richtext(msg.text )
    if msg.text != text:
        msg.text = text 
        if metadata:
            #fix spidd_replies
            if 'spidd_replies' in metadata:
                handle_spidd_replies(msg,metadata)

            metatext = msg.metadata if msg.metadata is not None else '{}'
            metadict = json.loads(metatext)
            metadict.update(metadata)
            msg.metadata = json.dumps(metadict)          
        msg.save()
   

def alter_incoming(msg=None):
    channel_type = msg.channel.channel_type if msg.channel else None
    numeric_result(msg)
     
    msg.save()
    pass
    
def numeric_result(msg):
    text = msg.text.strip() 
    # number between 0 - 99
    if not text.isdigit() or len(text) > 2:
        return
    print 'Channel is Kannel'
    prev_msg = Msg.objects.filter(direction='O', contact=msg.contact).order_by('-created_on').first()
    if not prev_msg or not prev_msg.metadata:
        return

    try:
        metaobject = json.loads(prev_msg.metadata)
    except ValueError, e:
        print '\ncant parse json\n'
        return 

    if 'quick_replies' not in  metaobject:  
        print 'quick replies missing' 
        return
    quick_replies = metaobject['quick_replies']
    if not isinstance(quick_replies, list) or len(quick_replies) < int(text):
        print 'not list'
        return

    msg.text = quick_replies[int(text)-1]
    print prev_msg.metadata if prev_msg.metadata else 'Nothing here'

